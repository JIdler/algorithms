/*
* Ex12
* Dynamically computes B(j,k) for the game where two players
* are given a list of positive integers and take turns picking either 
* either the first or last value in the row and removing that value.
*
* B(j,k) is the maximum score possible, assuming each player plays optimally 
*
* Joey Idler
*
*/

import java.util.Scanner;
import java.util.Random;

public class Ex12
{

	private int[] v;	
	private int[][] chart;
	public int count; 

	public Ex12( int[] items )
	{
		v = items;
		chart = new int[v.length][v.length];

		for( int j = 0; j < chart.length; j++ )
		{
			for( int k = 0; k < chart[j].length; k++ )
				chart[j][k] = 0;
		}
		count = 0;
	}

	// Dynamically compute B(j,k)
	public int dynB( int j, int k )
	{
		count++; 

		if( j > k )
		{
			return 0;
		}
		else if( j == k ) {

			return v[j];
		}
		else if( chart[j][k] == 0 )
		{
			/*
			if(  j == k )
			{
				chart[j][k] = v[j];
			}
			else
			{ */
				chart[j][k] = Math.max( v[j] + Math.min( dynB( j+2, k ), 
						dynB( j+1, k-1 ) ), v[k] + Math.min( dynB( j, k-2 ), 
						dynB( j+1, k-1 ) ) );
				chart[j][k-1]= dynB(j,k-1);
				chart[j+1][k]= dynB(j+1,k);
		//	}


		}

		return chart[j][k];	

	}

	public void printChart()
	{
		System.out.println();

		for( int j = 0; j < chart.length; j++ )
		{
			for( int k = 0; k < chart[j].length; k++ )
				System.out.print( chart[j][k] + " " );

			System.out.println();
		}

		System.out.println("Number of calls: " + count);
	}

	public static void main( String[] args )
	{
		int bestScore = 0;

		int n;
		Scanner input = new Scanner( System.in );
		System.out.print( "Enter number of items (should be even): " );
		n = input.nextInt();
		input.nextLine();

		int[] items = new int[n];
		System.out.print( "Enter the " + n + " items: " );
		for( int i = 0; i < n; i++ )
			items[i] = input.nextInt();
		input.nextLine();
/*
		Random random = new Random();
		for( int i = 0; i < n; i++ )
		{
			items[i] = random.nextInt(10) + 1;
			System.out.print( items[i] + " " );
		}
		System.out.println();
*/
		Ex12 test = new Ex12( items );

		bestScore += test.dynB( 0, n-1 );


		System.out.println( "Best score = " + bestScore );

		test.printChart();

	}

}
