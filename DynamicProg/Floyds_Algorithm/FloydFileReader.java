/*
* FloydFileReader.java
*
* Helper class for Floyd.java
* Reads DiGraph data from file
*
* Joey Idler
*/

import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.io.File;
import java.io.FileNotFoundException;

public class FloydFileReader {

	public static List<List<Tuple>> readFile(final String file) {

		try {
			Scanner fileScanner = new Scanner(new File(file));

			// Create 2D ArrayList
			List<List<Tuple>> table = new ArrayList<List<Tuple>>();
	
			while(fileScanner.hasNextLine()) {
	
				table.add(new ArrayList<Tuple>());
				int index = table.size() - 1;

				// Reads individual lines from file
				Scanner lineScanner = new Scanner(fileScanner.nextLine());

				while(lineScanner.hasNextInt()) {
	
					Tuple t = new Tuple(lineScanner.nextInt());
					table.get(index).add(t);
				}
			
				//clean up 
				lineScanner.close();
			}

			//clean up
			fileScanner.close();

			// return
			return table;

		} catch(FileNotFoundException e) {

			e.printStackTrace();
			System.exit(100);
		}

		return null;
	}

}
