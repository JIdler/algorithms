/*
* Tuple.java
*
* Simple Tuple utility class for Floyd.java 
*
* Joey Idler
*/

public class Tuple {

	// w = weight
	// v = vertice
	public int w,v;

	public Tuple(int w) {

		this.w = w;
		v = 0;
	}

}
