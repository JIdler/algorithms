/*
* Implementation of Floyds algorithm
*
*
*
* Joey Idler
*
*/

import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;

public class Floyd
{

	// Number of vertices in graph
	private int size;
	// Graph implemented as 2D ArrayList
	private List<List<Tuple>> table;

        // Constructor
	public Floyd( List<List<Tuple>> table)
	{
		this.table = table;
		size = this.table.size();
	}

	/*
	* getPaths method
	*
	* Prints tables D^0,D^1,...,D^k
	*/
	public void getPaths()
	{
		// Print base case table (D^0)
		printTable(0);

                // Table where vertices V1 through Vk 
		// are allowed to be used
		for( int k = 1; k <= size; k++ )
		{
		        // i - row i of table D^k
			for( int i = 0; i < size; i++ )
			{
			        // j - column j of table D^k
				for( int j = 0; j < size; j++ )
				{
				        // When i == j, weight is
					// 0 of course so nothing to do
					if( i != j )
						table.get(i).set( j, d( i, j, k ) );
				}
			}
			
			// Print table D^k
			printTable(k);
		}
	}

	/*
	* d method:
	* Calculates D^k(i,j), the best path from i to j
	* using vertices V1 through Vk
	*
	* i - row
	* j - column
	* k - intermediate vertex to use
	*
	* returns - Tuple containing the computed weight, and 
	* 	intermediate vertex that produces it
	*/
	public Tuple d( int i, int j, int k )
	{
	        // get stored weight and intermediate 
		// vertex stored in row i column j
		Tuple t = table.get(i).get(j);

		if( k == 0 )
			return t;
		else
		{
			// optimal length calculated so far
			int optLength = t.w;
			// calculated weights from i to k-1
			// and k-1 to j
			int iTok = table.get(i).get(k-1).w;
			int kToj = table.get(k-1).get(j).w;

			if(  iTok == -1 || kToj == -1 )
				return t;
			else
			{       
				// shorter length found
				// set newly calculated weight and
				// intermediate vertex, then return tuple
				if( (iTok + kToj) < optLength || optLength == -1)
				{
					t.w = (iTok + kToj);
					t.v = k;
				}
				return t; 
			}
		}
		
	}

        /*
	* Helper method to print graph weights 
	* in a somewhat formated matter.
	*/
	public void printTable(final int k)
	{
		System.out.println("---------------------------------------------");
		System.out.println( "D" + k + "-" );
		for( int i = 0; i < size; i++ )
		{
			for( int j = 0; j < size; j++ )
			{
	
				Tuple t = table.get(i).get(j);

				if( t.w == -1)
				{	
					System.out.printf( "%2c", '-');
					System.out.printf( "(%2d) ", t.v );
				}
				else
				{
					System.out.printf("%2d", t.w );
					System.out.printf("(%2d) ", t.v );
				}
			}
                        
			System.out.println();
			System.out.println();
		}

		System.out.println("---------------------------------------------");
		System.out.println();
	}



public static void main(String[] args)
{
        // Scanner to get user input
	Scanner keys = new Scanner( System.in );
	// number of vertices in directed graph
	int numVerts; 
	// Graph implemented as 2D ArrayList
	List<List<Tuple>> table;

        // Get number of vertices from user
	System.out.print( "Enter number of vertices: " );
	numVerts = keys.nextInt();

        // init outer ArrayList
	table = new ArrayList<List<Tuple>>( numVerts );
	for( int i = 0; i < numVerts; i++ )
		table.add( new ArrayList<Tuple>() );


	for( int i = 0; i < numVerts; i++ )
	{
		for( int j = 0; j < numVerts; j++ )
		{       
				// Get weights from user
				// -1 indicates no edge between vertices
				System.out.print( "Enter weight from " + (i+1) + 
						" to " + (j+1) + ", or -1 if " +
						"no edge between vertices: " );
				table.get(i).add(new Tuple(keys.nextInt()));
		}
	}
/*
* Code for Testing purposes!
* Read weights from file rather than entering each one manually
*
*	System.out.print("Enter file name: ");
*	
*	table = FloydFileReader.readFile(keys.nextLine());
*/

	// Call Floyd constructor with data
	Floyd test = new Floyd(table);

        // Calculate and display tables
	test.getPaths();
}

}







