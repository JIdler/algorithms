/*
* Ex17.java
* 
* Solve instance of 0-1 Knapsack problem
* using the branch and bound method
*
* Joey Idler
*/

import java.util.List;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.Collections;
import java.io.File;
import java.io.FileNotFoundException;

public class Ex17 {

	// inner classes

	/*
	* Node class
	*
	*/
	private class Node {

		int iD; // node id
		List<Item> knapsack = new ArrayList<Item>();
		int level; // nodes level in tree
		int profit; // total profit of items in node
		int weight; // total weight of items in node
		float bound; // bound for this node
	} // end Node

	/*
	* Item class
	*
	*/
	public class Item implements Comparable<Item> {

		int i; // item number
		final int p; // item profit
		final int w; // item weight

		// Item constructor
		public Item(final int i, final int p, final int w) {

			this.i = i;
			this.p = p;
			this.w = w;
		}

		@Override
		public int compareTo( Item item ) {

			return (int)(((Item)item).p / ((Item)item).w) - (int)(this.p / this.w); 
		}
	} // end Item

	// instance variables
	private final int cap; // capacity of knapsack
	private final int n; // number of items to choose from
	private int nodeCount = 1; // keep track of next node iD 

	private List<Item> items; // all items to choose from

	private Node bestNode;	// hold best node found so far 

	private PriorityQueue<Node> pQueue; // priority queue for nodes 

	// Ex17 constructor
	public Ex17( final int cap, final int n, final int[] p, final int[] w ) {

		this.cap = cap;
		this.n = n;
		this.items = new ArrayList<Item>( n );

		for( int i = 0; i < n; i++ ) 
			this.items.add( new Item( i + 1, p[i], w[i] ) );

		// initialize empty PriorityQueue with Comparator
		pQueue = new PriorityQueue<Node>( n, new Comparator<Node>() {
				public int compare( Node n1, Node n2 ) {
					if( n1.bound > n2.bound ) {
						return -1;
					} else if( n1.bound < n2.bound ) {
						return 1;
					} 

					return ( n1.iD < n2.iD ) ? -1 : 1;
				}
			});
	}

	// solve knapsack problem using branch and bound method
	public void branchBoundKnapsack() {

		System.out.println( "\nCapacity of knapsack is " + cap );

		System.out.println( "Items are:" );
		for(Item item : items) 
			System.out.println( item.i + ": " + item.p + " " + item.w );

		// make sure items are in the correct order
		System.out.printf( "\n\nSorting...\n\n" );
		this.sortItems();
		System.out.printf( "Items sorted by most profitable " + 
						"per unit of weight, in descending order\n" );
		for(Item item : items)
			System.out.println( item.i + ": " + item.p + " " + item.w );

		System.out.printf( "\n\nBegin exploration of the possibilities tree:\n\n" );

		// initialize topNode as root
		Node topNode = new Node();
		topNode.level = 0; topNode.profit = 0; topNode.weight = 0;
		topNode.iD = nodeCount; 
		nodeCount++;

		topNode.bound = setBound( topNode );

		pQueue.add( topNode );

		bestNode = topNode;

		// explore nodes in queue
		while( !pQueue.isEmpty() ) {

			// remove next node to explore
			topNode = pQueue.remove();

			if( topNode.bound < bestNode.profit ) {

				printNode( "Exploring", topNode );
				System.out.printf( "\tpruned, dont explore because bound %.2f" +
						" is smaller than the known achievable profit %2d\n\n", 
						topNode.bound, bestNode.profit );
			}
			else 
				branch( topNode );

		}

		// print best node info
		printNode( "Best node:", bestNode );
	}

	/*
	* branch from node
	*
	*/
	private void branch( Node topNode ) {

		Node leftNode, rightNode;

		printNode( "Exploring", topNode );
		
		// initialize leftNode (doesn't include next item)
		leftNode = new Node();
		leftNode.knapsack.addAll( topNode.knapsack );
		leftNode.level = topNode.level + 1; 
		leftNode.profit = topNode.profit; 
		leftNode.weight = topNode.weight;
		leftNode.iD = nodeCount;
		nodeCount++;

		// calculate bound for left node
		leftNode.bound = setBound( leftNode );

		printNode( "Left child is", leftNode );

		// add left node to queue if bound is greater 
		// than the current known max profit
		// and max depth hasn't been reached
		if( leftNode.bound < bestNode.profit ) {

	               	System.out.printf( "\t\tpruned, dont explore because bound %.2f" +
				" is smaller than the known achievable profit %2d\n", 
				leftNode.bound, bestNode.profit );
		}
		else {

			if( leftNode.level == n ) {

				System.out.printf( "\t\treached max depth, " + 
							"cannot explore further\n" );
			}
			else {

				pQueue.add( leftNode );

				System.out.printf( "\t\tExplore further\n" );
			}
		}

		// initialize rightNode (includes next item)
		rightNode = new Node();
		rightNode.level = topNode.level + 1;
		rightNode.knapsack.addAll( topNode.knapsack );
		rightNode.knapsack.add( items.get( rightNode.level - 1 ) );
		rightNode.weight = topNode.weight + items.get( rightNode.level - 1 ).w;
		rightNode.profit = topNode.profit + items.get( rightNode.level - 1 ).p;
		rightNode.iD = nodeCount;
		nodeCount++;

		// calculate bound for right node
		rightNode.bound = setBound( rightNode );

		printNode( "Right child is", rightNode );

		// set rightNode as bestNode or prune
		if( rightNode.weight <= cap && rightNode.level <= n ) {

				if( rightNode.weight == cap ) {

					System.out.printf( "\t\thit capacity exactly " + 
								"so dont explore further\n" );
				}
				else if( rightNode.level == n ) {

					System.out.printf( "\t\treached max depth, " + 
								"cannot explore further\n" );
				}
				else {
					// add to queue since it can still be explored
					pQueue.add( rightNode );

					System.out.printf( "\t\tExplore further\n" );
				}

				if( bestNode.profit < rightNode.profit ) {

					System.out.printf( "\t\tnote achievable profit of %2d\n", 
										rightNode.profit );
					bestNode = rightNode;
				}

				System.out.println();
		}
		else
			System.out.printf( "\t\tpruned because too heavy\n\n" );
		
	}

	// calculate and set bound for node
	private float setBound( Node node ) {

		int level = node.level; // nodes level
		int weight = node.weight; // accumulated weight
		float bound = node.profit; // bound

		if( level == n )
			return bound;

		// get as many items as possible
		while( (level < n) && (weight + items.get( level ).w <= cap) )
		{
			Item item = items.get( level );
			weight += item.w;
			bound += item.p;

			level++;
		}

		if( level < n && weight <= cap )
			bound += (cap - weight) * 
				( (double)items.get( level ).p / (double)items.get( level ).w );

		return bound;

	}

	// helper function to print the node information 
	public void printNode( String s, Node node )
	{
		String containsItems = "["; 

		for( int i = 0; i < node.knapsack.size(); i++ )
		{
			if( (i + 1) < node.knapsack.size() )
				containsItems += Integer.toString( node.knapsack.get(i).i ) + ", ";
			else
				containsItems += Integer.toString( node.knapsack.get(i).i ); 
		}
		containsItems += "]";

		if( !s.equals("Exploring") && !s.equals("Best node:") )
			System.out.printf("\t");

		System.out.printf( "%s <node %1d: items: %s level: %1d profit: %1d weight: %1d bound: %.2f>\n",
				s, node.iD, containsItems, node.level, node.profit, 
				node.weight, node.bound );
	}

	// sorts items in order from most profitable per unit of weight 
	// to least profitable
	private void sortItems() {

		Collections.sort( this.items );

		int n = 1;

		for( Item item : this.items ) {

			item.i = n;
			n++;
		}
	}

	public static void main(String[] args)
	{
		if( args.length == 0 )
		{
			System.out.println( "Usage: java Ex17 <data file name>" );
			System.exit(1);
		}

		String filename = args[0];

		try {

			Scanner	input = new Scanner( new File( filename ) );
	
			// get capacity from file
			int cap = input.nextInt(); 

			// get number of items from file
			input.nextLine();
			int n = input.nextInt(); 

			// get profit and weight for each item
			input.nextLine();
			int[] p = new int[n]; int[] w = new int[n];

			for( int i = 0; i < n; i++ ) {

				p[i] = input.nextInt(); w[i] = input.nextInt();

				input.nextLine();
			}

			Ex17 knapsack = new Ex17( cap, n, p, w );

			knapsack.branchBoundKnapsack();
		}
		catch( FileNotFoundException e ) {

			System.err.println( "Problem loading data file: " + e.getMessage() );
			System.exit(1);
		}

	} // end main
}
