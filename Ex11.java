/*
* Ex11
* Dynamically computes B(j,k) for the game where two players
* are given a list of positive integers and take turns picking either 
* either the first or last value in the row and removing that value.
*
* B(j,k) is the maximum score possible, assuming each player plays optimally 
*
* Joey Idler
*
*/

import java.util.Scanner;
import java.util.Random;

public class Ex11
{

	private int[] v;	
	private int[][] chart;
	public int count; 

	public Ex11( int[] items )
	{
		v = items;
		chart = new int[v.length][v.length];

		for( int j = 0; j < chart.length; j++ )
		{
			for( int k = 0; k < chart[j].length; k++ )
				chart[j][k] = 0;
		}
		count = 0;
	}

	// Dynamically compute B(j,k)
	public int dynB( int j, int k )
	{
		count++; 

		if( j > k )
		{
			return 0;
		}
		else if( chart[j][k] == 0 )
		{
			if(  j == k )
			{
				chart[j][k] = v[j];
			}
			else
			{
				chart[j][k] = Math.max( v[j] + Math.min( dynB( j+2, k ), 
						dynB( j+1, k-1 ) ), v[k] + Math.min( dynB( j, k-2 ), 
						dynB( j+1, k-1 ) ) );
			}


		}

		return chart[j][k];	

	}

	// Compute B(j,k) using brute force.
	// Takes 2^n time so only use for small n.
	// I left this in here because I used it to make sure my dynB 
	// method was working correctly. 
	public int b( int j, int k)
	{
		count++; 

		if( k < j )
		{
			return 0;
		}
		else
		{
			return Math.max( v[j] + Math.min( b( j+2, k ), b( j+1, k-1 ) ),
						v[k] + Math.min( b( j, k-2 ), b( j+1, k-1 ) ) ); 
		}
		
	}

	public void printChart()
	{
		System.out.println();

		for( int j = 0; j < chart.length; j++ )
		{
			for( int k = 0; k < chart[j].length; k++ )
				System.out.print( chart[j][k] + " " );

			System.out.println();
		}
	}

	public static void main( String[] args )
	{
		int bestScore = 0;

		int n;
		Scanner input = new Scanner( System.in );
		System.out.print( "Enter number of items (should be even): " );
		n = input.nextInt();
		input.nextLine();

		int[] items = new int[n];
		System.out.print( "Enter the " + n + " items: " );
		for( int i = 0; i < n; i++ )
			items[i] = input.nextInt();
		input.nextLine();
/*
		Random random = new Random();
		for( int i = 0; i < n; i++ )
		{
			items[i] = random.nextInt(10) + 1;
			System.out.print( items[i] + " " );
		}
		System.out.println();
*/
		Ex11 test = new Ex11( items );

		bestScore += test.dynB( 0, n-1 );

		System.out.println( "Best score = " + bestScore );

		test.printChart();

	}

}
