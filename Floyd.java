/*
* Implementation of Floyds algorithm
*
*
*
* Joey Idler
*
*/

import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;

public class Floyd
{

	private int nVerts;
	private List<List<Integer>> weights;
	private int[][]  interVerts;
	private int count;

	public Floyd( List<List<Integer>> weights, int n )
	{
		this.weights = weights;
		nVerts = n;
		count = 0;

		interVerts = new int[nVerts][nVerts];
		for( int i = 0; i < nVerts; i++ )
		{
			for( int j = 0; j < nVerts; j++ )
				interVerts[i][j] = 0;
		}
	}

	public void shortestPath()
	{
		printTable();

		for( int k = 1; k <= nVerts; k++ )
		{
			for( int i = 0; i < nVerts; i++ )
			{
				for( int j = 0; j < nVerts; j++ )
				{
					if( i != j )
						weights.get(i).set( j, d( i, j, k ) );
				}
			}
			printTable();
		}
	}

	public int d( int i, int j, int k )
	{
		
		if( k == 0 )
			return weights.get(i).get(j);
		else
		{
			int bestScore = weights.get(i).get(j);
			int iTok = d( i, k-1, 0 );
			int kToj = d( k-1, j, 0 );

			if(  iTok == -1 || kToj == -1 )
				return weights.get(i).get(j);
			else
			{
				if( (iTok + kToj) < bestScore || bestScore == -1)
				{
					bestScore = (iTok + kToj);
					interVerts[i][j] = k;
				}
				return bestScore; 
			}
		}
		
	}

	public void printTable()
	{
		System.out.print( "D" + count + "-" );
		System.out.println();
		for( int i = 0; i < nVerts; i++ )
		{
			for( int j = 0; j < nVerts; j++ )
			{
				if( weights.get(i).get(j) == -1)
				{	
					System.out.printf( "%2c", '-');
					System.out.printf( "(%2d)", interVerts[i][j] );
				}
				else
				{
					System.out.printf("%2d", weights.get(i).get(j) );
					System.out.printf("(%2d)", interVerts[i][j] );
				}
			}

			System.out.println();
		}

		System.out.println();
		System.out.println();
		count++;
	}



public static void main(String[] args)
{
	Scanner keys = new Scanner( System.in );
	int numVerts; 
	List<List<Integer>> diGraph;

	System.out.print( "Enter number of vertices: " );
	numVerts = keys.nextInt();

	diGraph = new ArrayList<List<Integer>>( numVerts );
	for( int i = 0; i < numVerts; i++ )
		diGraph.add( new ArrayList<Integer>() );

	for( int i = 0; i < numVerts; i++ )
	{
		for( int j = 0; j < numVerts; j++ )
		{       
/*			if( i == j )
				diGraph.get(i).add( 0 );
			else
			{
*/				System.out.print( "Enter weight from " + (i+1) + 
						" to " + (j+1) + ", or -1 if " +
						"no edge between vertices: " );
				diGraph.get(i).add( keys.nextInt() );
//			}
		}
	}

	Floyd test = new Floyd( diGraph, numVerts );

	test.shortestPath();
}

}







