/*
* Joey Idler
* 
* Solve instance of 0-1 Knapsack problem
* using the branch and bound method
*/

import java.util.*;
import java.io.*;

public class Ex17
{
	public static void main(String[] args)
	{
		if( args.length == 0 )
		{
			System.out.println( "Usage: java Ex17 <data file name>" );
			System.exit(1);
		}

		Ex17 knap = new Ex17( args[0] );

		knap.bbKnapsack();
	}

	// instance variables:
	
	private int cap; // capacity of knapsack
	private int num; // number of items
	private int n; // keeps track of current greatest node number

	private int[] p; // item profit
	private int[] w; // item weight

	private Node root; // root node
	private Node bestNode; 

	private List<Node> queue; // queue to hold nodes that are to be further explored

	public Ex17( String filename )
	{
		try
		{
			Scanner input = new Scanner( new File( filename ) );

			cap = input.nextInt(); 
			System.out.println( "Capacity of knapsack is " + cap );

			input.nextLine();

			num = input.nextInt(); 
			input.nextLine();

			p = new int[num]; w = new int[num];

			for( int i = 0; i < num; i++ )
			{
				p[i] = input.nextInt();
				w[i] = input.nextInt();

				input.nextLine();
			}

			System.out.println( "Items are:" );
			for( int i = 0; i < num; i++ )
				System.out.println( (i + 1) + ": " + p[i] + " " + w[i] );

			System.out.println();

		}
		catch( Exception e )
		{
			System.out.println( "Problem loading data file <" + filename + ">" );
			System.exit(1);
		}

		queue = new ArrayList<Node>();
	
		n = 1;
	}

	// solve knapsack problem using branch and bound
	public void bbKnapsack()
	{
		// create root node
		root = new Node( null, n, 0, 0, 0 ); 
		n++; // update n

		System.out.printf( "Begin exploration of the possibilities tree:\n\n" );

		bound( root );

		bestNode = root;

		branch( root );

		// explore nodes in queue
		while( !queue.isEmpty() )
		{
			Node node = queue.remove(0);

		
			if( node.getBound() < bestNode.getProfit() )
			{
				printNode( "Exploring", node );
				System.out.printf( "\tpruned, dont explore because bound %.1f" +
						" is smaller than the known achievable profit %2d\n", 
						node.getBound(), bestNode.getProfit() );
			}
			else
				branch( node );
		
		}

		// print best node info
		printNode( "Best node:", bestNode );
	}

	public void branch( Node node )
	{
		Node leftNode = node.newChild( n, true ); // left child
		n++; // update n

		Node rightNode = node.newChild( n, false ); // right child
		n++; // update n

		printNode( "Exploring", node );
		
		// calculate bound for left child
		bound( leftNode );

		// for right child: add item, calculate profit and weight
		int level = rightNode.getLevel();

		rightNode.addItem( level );

		rightNode.addProfit( p[level - 1] );

		rightNode.addWeight( w[level - 1] );

		// calculate bound for right child
		bound( rightNode );

		printNode( "Left child is", leftNode );

		if( leftNode.getWeight() <= cap && leftNode.getLevel() <= num )
		{
			if(  leftNode.getWeight() == cap )
				System.out.printf( "\t\thit capacity exactly so dont explore further\n" );
			else if( leftNode.getLevel() == num )
				System.out.printf( "\t\treached max depth, cannot explore further\n" );
			else if( leftNode.getBound() < bestNode.getProfit() )
	               		System.out.printf( "\t\tpruned, dont explore because bound %.1f" +
					" is smaller than the known achievable profit %2d\n", 
					leftNode.getBound(), bestNode.getProfit() );
			else
				System.out.printf( "\t\tExplore further\n" );

			if( leftNode.getProfit() > rightNode.getProfit() )
				System.out.printf( "\t\tnote achievable profit of %2d\n", leftNode.getProfit() );
		}
		else if( leftNode.getWeight() > cap )
			System.out.printf( "\t\tpruned because too heavy\n" );

		printNode( "Right child is", rightNode );

		if( rightNode.getWeight() <= cap && rightNode.getLevel() <= num )
		{
			if( rightNode.getWeight() == cap )
				System.out.printf( "\t\thit capacity exactly so dont explore further\n" );
			else if( rightNode.getLevel() == num )
				System.out.printf( "\t\treached max depth, cannot explore further\n" );
			else if( rightNode.getBound() < bestNode.getProfit() )
                                System.out.printf( "\t\tpruned, dont explore because bound %.1f" +
                                                 " is smaller than the known achievable profit %2d\n",
                                                 rightNode.getBound(), bestNode.getProfit() );
			else
				System.out.printf( "\t\tExplore further\n" );

			if( rightNode.getProfit() > leftNode.getProfit() )
				System.out.printf( "\t\tnote achievable profit of %2d\n", rightNode.getProfit() );
			System.out.println();
		}
		else if( rightNode.getWeight() > cap )
			System.out.printf( "\t\tpruned because too heavy\n\n" );
		
		if( leftNode.getWeight() <= cap && rightNode.getWeight() <= cap )
		{
			if( leftNode.getProfit() > rightNode.getProfit() )
			{

				queue.add( rightNode );

				if( leftNode.getBound() > bestNode.getProfit() )
				{ 
					if( leftNode.getProfit() > bestNode.getProfit() )
						bestNode = leftNode;
			

					if( leftNode.getWeight() < cap && leftNode.getLevel() < num )
						branch( leftNode );
				}
				
				
			}
			else
			{

				queue.add( leftNode );

				if( rightNode.getBound() > bestNode.getProfit() )
				{ 
					if( rightNode.getProfit() > bestNode.getProfit() )
						bestNode = rightNode;


					if( rightNode.getWeight() < cap && rightNode.getLevel() < num )
						branch( rightNode );
				}
				
					
			}
		}
		else if( leftNode.getWeight() > cap && rightNode.getWeight() <= cap )
		{
			if( rightNode.getBound() > bestNode.getProfit() )
			{
				if( rightNode.getProfit() > bestNode.getProfit() )
					bestNode = rightNode;

				if( rightNode.getWeight() < cap && rightNode.getLevel() < num )
					branch( rightNode );
			}
		}
		else if( leftNode.getWeight() <= cap && rightNode.getWeight() > cap )
		{
			if( leftNode.getBound() > bestNode.getProfit() )
			{
				if( leftNode.getProfit() > bestNode.getProfit() )
					bestNode = leftNode;

				if( leftNode.getWeight() < cap && leftNode.getLevel() < num )
					branch( leftNode );
			}
		}

	}

	// calculate and set bound for node
	public void bound( Node node )
	{
		int weight = node.getWeight(); // accumulated weight
		double b = node.getProfit(); // bound
		int l = node.getLevel();

		while( (l < num) && (weight + w[l] <= cap) )
		{
			weight += w[l];
			b += p[l];

			l++;
		}
		if( l < num )
			b += (cap - weight)*( (double)(p[l]) / (double)(w[l]));

		node.setBound( b );

	}

	// helper function to print the node information 
	public void printNode( String s, Node node )
	{
		String items = "["; 

		for( int i = 0; i < node.getItems().size(); i++ )
		{
			if( (i + 1) < node.getItems().size() )
				items += Integer.toString( node.getItems().get(i) ) + ", ";
			else
				items += Integer.toString( node.getItems().get(i) ); 
		}
		items += "]";

		if( !s.equals("Exploring") && !s.equals("Best node:") )
			System.out.printf("\t");

		System.out.printf( "%s <node %1d: items: %s level: %1d profit: %1d weight: %1d bound: %.1f>\n",
				s, node.getN(), items, node.getLevel(), node.getProfit(), 
				node.getWeight(), node.getBound() );
/*
                  System.out.println( s + " <Node " + node.getN() + ": items: " + node.getItems() +
                                   " level: " + node.getLevel() + " profit: " + node.getProfit() +
                                    " weight: " + node.getWeight() + " bound: " + node.getBound()  + ">" );
*/
	}

}
