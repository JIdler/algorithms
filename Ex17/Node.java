/*
* Joey Idler
*
* Node class for Ex17
*/

import java.util.*;

public class Node
{
	private int n; // node number
	private List<Integer> items; // items selected at this node
	private int level; // level of this node in the tree
	private int profit; // profit for this node
	private int weight; // weight for this node
	private double bound; // bound on this node
	private Node lChild; // left child
	private Node rChild; // right child

	// constructor 
	public Node( List<Integer> i, int id, int l, int p, int w )
	{
		if( i == null )
			items = new ArrayList<Integer>();
		else
		{
			items = new ArrayList<Integer>();

			for( int k = 0; k < i.size(); k++ )
				items.add( i.get(k) );
		}

		n = id;
		level = l;
		profit = p;
		weight = w;

	}

	public Node newChild( int id, boolean isLeft )
	{
			if( isLeft )
			{
				lChild = new Node( this.getItems(), id, 
					this.getLevel() + 1, this.getProfit(), this.getWeight() );


				return lChild;
			}
			else
			{
				rChild = new Node( this.getItems(), id, 
					this.getLevel() + 1, this.getProfit(), this.getWeight() );


				return rChild;
			}
	}

	public List<Integer> getItems()
	{
		return this.items;
	}

	public void addItem( int item )
	{
      		this.items.add( item );
	}

	public int getN()
	{
		return this.n;
	}

	public void addProfit( int p )
	{
		this.profit += p;
	}

	public int getProfit()
	{
		return this.profit;
	}

	public int getLevel()
	{
		return this.level;
	}

	public void addWeight( int w )
	{
		this.weight += w;
	}

	public int getWeight()
	{
		return this.weight;
	}

	public void setBound( double b )
	{
		this.bound = b;
	}

	public double getBound()
	{
		return this.bound;
	}

}
